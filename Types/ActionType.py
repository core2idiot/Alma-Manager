from enum import Enum
class ActionType(Enum):
    PROVENANCE_CODE = 1
    PERMANENT_LOCATION = 2
    SUPPRESS = 3

