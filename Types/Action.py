class Action(object):
    def __init__(self, Type, MachineReadable, HumanReadable=None):
        self.Type = Type
        self.MachineReadable = MachineReadable
        if HumanReadable is not None:
            self.HumanReadable = HumanReadable
        else:
            self.HumanReadable = MachineReadable
            
