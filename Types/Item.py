import xml.etree.ElementTree as ET
"""
   This class takes an ElementTree and creates an object that
   can be addressed using human readable elements
"""
class Item():
    def __init__(self, m_Item, OtherLibraryInformation=None, Suppression=None):
        try:
#           self.name = m_Item[0][1].text'"
            self.name = m_Item[0].find('title').text
        except Exception:
            self.name = "name"
#           self.author = m_Item[0][2].text
        try:
            self.author = m_Item[0].find('author').text
        except Exception:
            self.author = "author"
#           self.mms_id = m_Item[0][0].text
        try:
            self.mms_id = m_Item[0].find('mms_id').text
        except Exception:
            self.mms_id = "mms_id"
#           self.holding_id = m_Item[1][0].text'
        try:
            self.holding_id = m_Item[1].find('holding_id').text
        except Exception:
            self.holding_id = "holding_id"
#           self.item_pid = m_Item[2][0].text
        try:
            self.item_pid = m_Item[2].find('pid').text
        except Exception:
            self.item_pid = "pid"
#           self.call_number = m_Item[1][2].text
        try:
            self.call_number = m_Item[1].find('call_number').text
        except Exception:
            self.call_number = "call_number"
        try:
            self.location = m_Item[2].find('location').get('desc')
        except Exception:
            self.location = "location"
        try:
            self.barcode = m_Item[2].find('barcode').text
        except Exception:
            self.barcode = "barcode"
        try:
            self.year = m_Item[2].find('year_of_issue').text
        except Exception:
            self.year = "year_of_issue"
        try:
            self.type = m_Item[2].find('physical_material_type').text
        except Exception:
            self.type = "type"
        try:
            self.provenance = m_Item[2].find('provenance').get('desc')
        except Exception:
            self.provenance = "provenance"
        self.AllData = m_Item
        try:
            self.Network = str(ET.tostring(m_Item[0].find('network_numbers')))
            self.Network = self.Network.replace('b\'<network_numbers>', '')
            self.Network = self.Network.replace('</network_number>', '')
            self.Network = self.Network.replace('</network_numbers>', '')
            self.Network = self.Network.replace('<network_number>', '\n')
            self.Network = self.Network[1:]
        except Exception:
            self.Network = "Network"
        self.Suppressed = Suppression
        self.OtherLibraryInformation = OtherLibraryInformation

    def update_item(self):
        self.AllData[2].find('location').text = self.location
        print(self.AllData[2].find('provenance').text)
        print(self.provenance)
        self.AllData[2].find('provenance').text = self.provenance

    def tostring(self):
        return ET.tostring(self.AllData)

    @staticmethod
    def IsItem(_item):
        if isinstance(_item, Item):
            return True
        return False


