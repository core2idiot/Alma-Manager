from PyQt5 import QtWidgets
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from API.sru import sru
import importlib

class LibrarySupportPackage(QtWidgets.QMenu):
    def __init__(self):
        super(LibrarySupportPackage, self).__init__()
        name = self.__module__.split(".")
        module = importlib.import_module(name[0] + ".__init__")
        self.Label = QtWidgets.QAction(name[0] + " : " + module.__version__)
        self.Label.setEnabled(False)
        self.addAction(self.Label)

    def FindCurrentAllianceHoldingsBYIZ_MMSID(self, mms_id):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
        return sru.FindCurrentAllianceHoldingsBYIZ_MMSID(mms_id, AlmaManagerConfiguration.instance.SRU_Institution, AlmaManagerConfiguration.instance.SRU_Alliance, AlmaManagerConfiguration.instance.SRU_URL)

    def findNZ_MMSIDByIZ_MMSID(self, mms_id):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
        return sru.findNZ_MMSIDByIZ_MMSID(mms_id, AlmaManagerConfiguration.instance.SRU_Institution, AlmaManagerConfiguration.instance.SRU_Alliance, AlmaManagerConfiguration.instance.SRU_URL)

    def GetSRUsearchRetrieveByMMSIDInstitution(self, mms_id):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
#       mms_id, Zone, SRU_URL
        return sru.GetSRUsearchRetrieveByMMSID(mms_id, AlmaManagerConfiguration.instance.SRU_Institution, AlmaManagerConfiguration.instance.SRU_URL)

    def GetSRUsearchRetrieveByMMSIDAlliance(self, mms_id):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
#       mms_id, Zone, SRU_URL
        return sru.GetSRUsearchRetrieveByMMSID(mms_id, AlmaManagerConfiguration.instance.SRU_Alliance, AlmaManagerConfiguration.instance.SRU_URL)

    def FindCurrentAllianceHoldingsBYNZ_MMSID(self, mms_id):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
        #NZ_MMSID, NZ_Name, SRU_URL
        return sru.FindCurrentAllianceHoldingsBYNZ_MMSID(mms_id, AlmaManagerConfiguration.instance.SRU_Alliance, AlmaManagerConfiguration.instance.SRU_URL)

    def GetSRUAttribute(self, SRUData, Findtag, Findcode):
        if not AlmaManagerConfiguration.instance.Enable_SRU:
            raise Exception("SRU is disabled")
        return sru.GetSRUAttribute(SRUData, Findtag, Findcode)

    def write_to_log(self, msg, level):
        name = self.__module__.split(".")
        if get_logger.instance:
            get_logger.instance.write_to_log(name[0] + " : " + msg, level)
        else:
            print(name[0] + " : " + msg)
