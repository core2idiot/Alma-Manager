from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from MainWindow.PanelView import PanelView
from MainWindow.Ribbon import Ribbon
from Administration.get_logger import get_logger
from Administration.AlmaManager import ManagerConfiguration
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from MainWindow.StatusBarSingleton import StatusBarSingleton

class mainWindow(QtWidgets.QWidget):
    def __init__(self):
        super(mainWindow, self).__init__()
        AlmaManagerConfiguration()
        self.Init_Alma()
        self.Init_UI()
        if AlmaManagerConfiguration.instance.DebugMode:
            self.setWindowTitle('Alma Manager - DebugMode')

        #self.ActionFetcher.start()

    def Init_Alma(self):
        ManagerConfiguration()
#            def refreshActionList(self):
        self.continous = False


#        self.m_alma.GetPrimoData("F")

    def Init_UI(self):
        try:
            self.setWindowTitle('Alma Manager')
            self.Window_Ribbon = Ribbon()
            self.Window_Layout = QtWidgets.QVBoxLayout()
            self.Window_Layout.addWidget(self.Window_Ribbon)
            self.Panel_View = PanelView()
            self.Window_Layout.addWidget(self.Panel_View)
            self.Status_Bar = StatusBarSingleton()
            self.Status_Bar.instance.addWidget(QtWidgets.QLabel("Ready"))
            self.Window_Layout.addWidget(self.Status_Bar.instance)
            self.Window_Ribbon.Refresh.connect(self.Panel_View.actions.ActionFetcher.start)
            self.Window_Ribbon.MultipleItemMode.connect(self.ToggleMultipleItemMode)
            self.Window_Layout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
            self.setLayout(self.Window_Layout)
            self.show()
            get_logger.instance.write_to_log("Created mainWindow", 8)

        except Exception as e:
            get_logger.instance.write_to_log('Error Creating UI: ' + str(e), 0)

    def ToggleMultipleItemMode(self, change):
        if change:
            self.Panel_View.actions.SetActionListCont()
        else:
            self.Panel_View.actions.SetActionListNotCont()



