from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from Administration.settingsWindow import SettingsWindow
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from MainWindow.StatusBarSingleton import StatusBarSingleton
from Types.LibrarySupportPackage import LibrarySupportPackage
import sys
import importlib
import os
from PyQt5.QtCore import pyqtSignal
#   Ribbon contains the settings and options at the top of the mainWindow
class Ribbon(QtWidgets.QWidget):
    Refresh = pyqtSignal()
    MultipleItemMode = pyqtSignal(bool)
    def __init__(self):
        try:
            super(Ribbon, self).__init__()
            self.LibrarySupportPackages = AlmaManagerConfiguration.instance.LibrarySupportPackages
            self.AdvancedMenus = []
            self.settingsBtn = QtWidgets.QPushButton("Settings")
            self.refreshBtn = QtWidgets.QPushButton("Refresh")
#            self.contBtn = QtWidgets.QPushButton("Multiple Item")
            self.AdvancedOptions = QtWidgets.QPushButton("Advanced Options")
            self.AdvancedOptionsMenu = QtWidgets.QMenu(self.AdvancedOptions)
            if AlmaManagerConfiguration.instance.MultipleItemModeEnabled:
                self.AdvancedOptionsMenu.addAction("Multiple Item Mode", self.ToggleMultipleItemMode)
            self.BuildAdvancedMenu()
            self.AdvancedOptions.setMenu(self.AdvancedOptionsMenu)
            self.layout = QtWidgets.QHBoxLayout()
            self.layout.addWidget(self.refreshBtn)
            if not AlmaManagerConfiguration.instance.Disable_Settings:
                self.layout.addWidget(self.settingsBtn)
            self.layout.addWidget(self.AdvancedOptions)
            self.settingsBtn.clicked.connect(self.settingsBtnAct)
#            self.contBtn.clicked.connect(self.contBtnAct)
            self.refreshBtn.clicked.connect(self.refreshBtnAct)
            self.setLayout(self.layout)
            self.setFixedHeight(50)
            if self.AdvancedOptionsMenu.isEmpty():
                self.AdvancedOptions.setDisabled(True)
            get_logger.instance.write_to_log("Rendered Ribbon", 7)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log("Error Creating Ribbon: " + str(e), 1)
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                              + str(exc_tb.tb_lineno), 5)

    def BuildAdvancedMenu(self):
        for each in self.LibrarySupportPackages:
            try:
                curr = importlib.import_module(each + ".__init__")
                temp = getattr(curr, "__name__")
                curr = importlib.import_module(each + "." + temp)
                temp = getattr(curr, temp)
                if issubclass(temp, LibrarySupportPackage):
                    cla = temp()
                    self.AdvancedMenus.append(cla)
                    self.AdvancedOptionsMenu.addMenu(self.AdvancedMenus[-1])
                    get_logger.instance.write_to_log("Added " + each + " to advanced menu", 7)
                else:
                    raise Exception("Attempted to import class that wasn't library support module")
            except Exception as e:
                temp = self.AdvancedOptionsMenu.addAction(each)
                temp.setEnabled(False)
                get_logger.instance.write_to_log("Error adding " + each + " to the advanced menu", 3)
                get_logger.instance.write_to_log(each + " : " + str(e), 1)

    def settingsBtnAct(self):
        try:
            self.settings = SettingsWindow()
            self.settings.show()

        except Exception as e:
            get_logger.instance.write_to_log('ERROR: Ribbon: Creating SettingsWindow: ' + str(e), 5)

    def ToggleMultipleItemMode(self):
        if AlmaManagerConfiguration.instance.MultipleItemMode is True:
            AlmaManagerConfiguration.instance.MultipleItemMode = False
            self.AdvancedOptionsMenu.actions()[0].setText("Multiple Item Mode")
            StatusBarSingleton.instance.showMessage("Entered Single Item Mode")
            self.MultipleItemMode.emit(False)
            get_logger.instance.write_to_log("Entering Single Item Mode", 7)

        else:

            AlmaManagerConfiguration.instance.MultipleItemMode = True
            #self.contBtn.setText('Single Item')
            self.AdvancedOptionsMenu.actions()[0].setText("Single Item Mode")
            self.MultipleItemMode.emit(True)
            get_logger.instance.write_to_log("Entering Multiple Item Mode", 7)
            StatusBarSingleton.instance.showMessage("Entered Multiple Item Mode")

    def refreshBtnAct(self):
        StatusBarSingleton.instance.showMessage("Refreshing Action Panel and Item")
        self.Refresh.emit()


