from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from PyQt5.QtCore import Qt
from Types.ActionType import ActionType
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from API.ActionFetcherThread import ActionFetcherThread
from API.ActionHandler import ActionHandler
from MainWindow.StatusBarSingleton import StatusBarSingleton
from MainWindow.MultipleItemModeConfirmationWindow import MultipleItemModeConfirmationWindow
from Types.Action import Action
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QSize
import sys
import os
import copy
#   ActionPanel creates a QWidget that contains a list of Actions.
#   This Class depends on AlmaConfiguration.
class ActionPanel(QtWidgets.QWidget):
    SizeChanged = pyqtSignal(int)
    def __init__(self):
        try:
            super(ActionPanel, self).__init__()
            self.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
            self.actionList = []
            self.CurrentItemCount = 0
            self.ActionHandler = ActionHandler()
            self.ActionHandler.ActionDone.connect(self.ActionDone)
            self.ActionHandler.ActionError.connect(self.ActionError)
            self.ActionFetcher = ActionFetcherThread()
            self.ActionFetcher.ActionsAvail.connect(self.refreshActionList)
            self.ActionFetcher.ActionsError.connect(self.ActionError)
            self.ActionFetcher.start()
            self.layout = QtWidgets.QVBoxLayout()
            self.layout.setAlignment(Qt.AlignLeft)
            self.setLayout(self.layout)
            self.Item = None
            self.NameOfAction = ""
            get_logger.instance.write_to_log("Populated List of Actions", 7)

        except Exception as e:
            get_logger.instance.write_to_log('Error populating list of actions: ' + str(e), 2)

    def ActionDone(self):
        StatusBarSingleton.instance.showMessage("Action Completed", 30000)

    def ActionError(self, error):
        StatusBarSingleton.instance.showMessage("Action Error: " + error, 30000)
        get_logger.instance.write_to_log("Action Error: " + error, 4)

    def SetActionListCont(self):
        self.SetActionEnabled(True)
        self.CurrentItemCount = 0
        for each in self.actionList:
            each.setCheckable(True)

    def SetActionListNotCont(self):
        self.SetActionEnabled(False)
        for each in self.actionList:
            each.setCheckable(False)

    def NewItem(self, item):
        self.SetActionEnabled(True)
        if self.Item is None or self.Item.barcode != item.barcode:
            self.Item = item
            if AlmaManagerConfiguration.instance.MultipleItemMode:
                if AlmaManagerConfiguration.instance.ConfirmItemMultipleItem:
                    self.MultipleItemModeWindow = MultipleItemModeConfirmationWindow(self.Item.barcode)
                    self.MultipleItemModeWindow.ItemAccepted.connect(self.doContAction)
                    self.MultipleItemModeWindow.show()
                else:
                    self.doContAction()
        elif AlmaManagerConfiguration.instance.MultipleItemMode:
                StatusBarSingleton.instance.showMessage("Action: " + self.NameOfAction + " performed " + str(self.CurrentItemCount)+ " Times," + " Item: " + self.Item.name, 30000)
        get_logger.instance.write_to_log("New Item in ActionPanel", 3)

#   This function deletes all items inside the ActionList and repopulates it
#   from AlmaConfiguration.  It does not guarantee that it is up to date, within
#   AlmaConfiguration.
    def refreshActionList(self, Actions):
        try:
            for each in self.actionList:
                self.layout.removeWidget(each)
                each.deleteLater()
                each = None
            del self.actionList[:]
            self.actionList = None
            self.actionList = []
            ActionsList = copy.deepcopy(Actions)
            for each in AlmaManagerConfiguration.instance.HiddenActions:
                for every in ActionsList:
                    try:
                        if each.MachineReadable.strip().lower() == every.MachineReadable.strip().lower() and each.Type == every.Type:
                            get_logger.instance.write_to_log("Removed Action " + each.MachineReadable, 5)
                            ActionsList.remove(every)
                    except Exception as e:
                        get_logger.instance.write_to_log("Error Removing Action, " + each.MachineReadable +": " + str(e), 4)

            for each in ActionsList:
                self.actionList.append(QtWidgets.QPushButton(str(each.HumanReadable)))
                self.actionList[-1].clicked.connect(self.findAction)
                self.actionList[-1].action = each
                self.actionList[-1].setCheckable(False)
                self.actionList[-1].setAutoExclusive(False)
                if each.Type == ActionType.PROVENANCE_CODE:
                    self.actionList[-1].setToolTip("Provenance Code")
                elif each.Type == ActionType.PERMANENT_LOCATION:
                    self.actionList[-1].setToolTip("Permanent Location")
                self.layout.addWidget(self.actionList[-1])
                get_logger.instance.write_to_log('Added ' + each.HumanReadable + ' to list of actions', 8)
            for each in AlmaManagerConfiguration.instance.KnownActions:
                self.actionList.append(QtWidgets.QPushButton(str(each.HumanReadable)))
                self.actionList[-1].clicked.connect(self.findAction)
                self.actionList[-1].action = each
                self.actionList[-1].setCheckable(False)
                self.actionList[-1].setAutoExclusive(False)
                if each.Type == ActionType.PROVENANCE_CODE:
                    self.actionList[-1].setToolTip("Provenance Code")
                elif each.Type == ActionType.PERMANENT_LOCATION:
                    self.actionList[-1].setToolTip("Permanent Location")
                self.layout.addWidget(self.actionList[-1])
                get_logger.instance.write_to_log('Added ' + each.HumanReadable + ' to list of actions', 8)
            if AlmaManagerConfiguration.instance.Enable_Suppress:
#    def __init__(self, Type, MachineReadable, HumanReadable=None):
                SupressAct = Action(ActionType.SUPPRESS, "Disconnect from Network and Suppress Bib")
                self.actionList.append(QtWidgets.QPushButton(SupressAct.HumanReadable))
                self.actionList[-1].clicked.connect(self.findAction)
                self.actionList[-1].action = SupressAct
                self.actionList[-1].setCheckable(False)
                self.actionList[-1].setAutoExclusive(False)
                self.layout.addWidget(self.actionList[-1])
            if not self.actionList:
                self.actionList.append(QtWidgets.QLabel(""))
            self.SetActionEnabled(False)
            self.SizeChanged.emit(self.actionList[0].width() - 300)
        except Exception as e:
            get_logger.instance.write_to_log("Error refreshActionList " + str(e), 7)

    def findAction(self):
        try:
            each = self.sender()
            if not AlmaManagerConfiguration.instance.MultipleItemMode:
                self.doAction(each)
            get_logger.instance.write_to_log('Reseting selection', 9)

        except Exception as e:
            get_logger.instance.write_to_log('Error finding button: ' + str(e), 2)
            #self.parent.Status_Bar.showMessage("Action: " + each.text() + " failed", 30000)

    def doContAction(self):
        try:
            for each in self.actionList:
                if each.isChecked():
                    self.doAction(each)

            get_logger.instance.write_to_log('Action ' + each.action.MachineReadable + ' has been done in multiple item mode ' + str(self.CurrentItemCount),4)
            self.CurrentItemCount += 1

        except Exception as e:
            get_logger.instance.write_to_log("Error finding button: " + str(e), 2)
            StatusBarSingleton.instance.showMessage("Error finding Button" + str(e), 30000)

    def SetActionEnabled(self, change):
        for each in self.actionList:
            each.setEnabled(change)

#   doAction takes a QPushButton and will conduct the action specified on the
#   on the push button.  button.type needs to be defined separately in order
#   for doAction to know what type of Action it is.  This is a member of type
#   ActionType.  Returns True if the action is performed, returns false if an
#   error occurs during the action
    def doAction(self, button):
        try:
            if self.Item is not None:
                self.SetActionEnabled(False)
                self.ActionHandler.wait()
                self.NameOfAction = button.action.HumanReadable
                self.ActionHandler.SetupAction(button.action, self.Item)
                self.ActionHandler.start()
                StatusBarSingleton.instance.showMessage("Performing Action: " + button.text(), 30000)
                #get_logger.instance.write_to_log('Changed ' + str(current_entry.name.encode('ascii', 'backslashreplace')) + ' to ' + button.text() , 4)
                return True
            else:
                return True

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                              + str(exc_tb.tb_lineno), 5)
            get_logger.instance.write_to_log('Error Conducting Action:' + button.text() + ' ' + str(e), 1)
            return False
 
