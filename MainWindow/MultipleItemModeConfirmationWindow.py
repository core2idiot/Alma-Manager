from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtCore import QThread, pyqtSignal

from Administration.get_logger import get_logger

class MultipleItemModeConfirmationWindow(QtWidgets.QWidget):
    ItemAccepted = pyqtSignal()
    def __init__(self, Barcode):
        super(MultipleItemModeConfirmationWindow, self).__init__()
        self.setWindowTitle("Confirm Action on Item")
        self.Barcode = Barcode
        self.resize(250, 150)
        self.layout = QtWidgets.QVBoxLayout()
        self.msg = QtWidgets.QLabel("Confirm Item: " + Barcode)
        self.BarcodeScan = QtWidgets.QLineEdit()
        self.BarcodeScan.returnPressed.connect(self.AcceptItem)
        self.CancelBarcode = QtGui.QImage("Resources/CancelAction.png")
        self.CancelBarcodeLabel = QtWidgets.QLabel("CancelBarcode")
        self.CancelBarcodeLabel.setPixmap(QtGui.QPixmap(self.CancelBarcode))
        self.layout.addWidget(self.msg)
        self.layout.addWidget(self.CancelBarcodeLabel)
        self.layout.addWidget(self.BarcodeScan)
        self.setLayout(self.layout)
        """
        We're trying to get our window on top of the stack but as per
        the Qt5 Documentation:
        "On Windows, if you are calling this when the application
        is not currently the active one then it will not make it
        the active window. It will change the color of the taskbar
        entry to indicate that the window has changed in some way.
        This is because Microsoft does not allow an application to
        interrupt what the user is currently doing in another application."
        """
        self.show()
        self.activateWindow()
        self.raise_()
        self.BarcodeScan.setFocus()
        self.BarcodeScan.selectAll()
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

    def AcceptItem(self):
        if self.BarcodeScan.text() == self.Barcode:
            self.ItemAccepted.emit()
            self.close()
            get_logger.instance.write_to_log("Item: " + self.Barcode + " Accepted", 5)
        elif self.BarcodeScan.text() == "Cancel Action":
            self.close()
        else:
            get_logger.instance.write_to_log("Item not confirmed", 1)
        
