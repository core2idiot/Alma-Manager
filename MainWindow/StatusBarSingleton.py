from PyQt5.QtCore import QMutex
from PyQt5.QtWidgets import QStatusBar
class StatusBarSingleton:
    class MyStatusBar(QStatusBar):
        def __init__(self):
            QStatusBar.__init__(self)
            self.__mutex = QMutex()
        def showMessage(self, Message, time = 15000):
            self.__mutex.lock()
            QStatusBar.showMessage(self, Message, time)
            self.__mutex.unlock()
    instance = None
    def __init__(self):
        if not StatusBarSingleton.instance:
            StatusBarSingleton.instance = StatusBarSingleton.MyStatusBar()
        else:
            raise Exception("No need to create an object after initialization")
