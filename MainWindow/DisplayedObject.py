from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from PyQt5.QtCore import Qt
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
#   DisplayedObject is a wrapper for the object returned from the Alma API
#   and contains places for the title, author and location
class DisplayedObject(QtWidgets.QWidget):
    def __init__(self):
        try:
            super(DisplayedObject, self).__init__()
            self.layout = QtWidgets.QVBoxLayout()
            self.layout.setAlignment(Qt.AlignLeft)
#           Define all of the labels that will contain the information
#           Obtained from the api.  This is changed from contentview
            self.title = QtWidgets.QLabel("Title")
            self.title.setToolTip("Title")
            self.author = QtWidgets.QLabel("Author")
            self.author.setToolTip("Author")
            self.location = QtWidgets.QLabel("Location")
            self.location.setToolTip("Location")
            self.type = QtWidgets.QLabel("Type")
            self.type.setToolTip("Type")
            self.provenance = QtWidgets.QLabel("Provenance")
            self.provenance.setToolTip("Provenance Code")
            self.Network = QtWidgets.QLabel("Network Numbers")
            self.Network.setToolTip("Network Numbers")
            self.Network.setTextInteractionFlags(Qt.TextSelectableByMouse)
            self.call_number = QtWidgets.QLabel("Call Number")
            self.call_number.setToolTip("Call Number")
            self.year = QtWidgets.QLabel("Year")
            self.year.setToolTip("Year")
#           Add all of the labels to the layout
            self.layout.addWidget(self.title)
            self.layout.addWidget(self.author)
            self.layout.addWidget(self.year)
            self.layout.addWidget(self.type)
            self.layout.addWidget(self.Network)
            self.layout.addWidget(self.call_number)
            self.layout.addWidget(self.provenance)
            self.layout.addWidget(self.location)
            if AlmaManagerConfiguration.instance.Display_MMS_ID is True:
                self.mms = QtWidgets.QLabel("MMS ID")
                self.mms.setToolTip("MMS ID")
                self.mms.setTextInteractionFlags(Qt.TextSelectableByMouse)
                self.layout.addWidget(self.mms)
            else:
                self.mms = None
            if AlmaManagerConfiguration.instance.Enable_Suppress is True:
                self.Suppressed = QtWidgets.QLabel("Suppression Status")
                self.Suppressed.setToolTip("Suppression Status")
                self.layout.addWidget(self.Suppressed)
            else:
                self.Suppressed = None
            if AlmaManagerConfiguration.instance.Enable_SRU is True:
                self.other_libraries = QtWidgets.QLabel("Other Holdings")
                self.other_libraries.setToolTip("Other Holdings")
                self.layout.addWidget(self.other_libraries)
            else:
                self.other_libraries = None
            self.setLayout(self.layout)

        except Exception as e:
            get_logger.instance.write_to_log('Error Displaying Object: ' + str(e), 5)



    def ShowObject(self, obj):
        self.current_entry = obj
        self.title.setText(self.current_entry.name)
        self.author.setText(self.current_entry.author)
        self.location.setText(self.current_entry.location)
        self.Network.setText(self.current_entry.Network)
        self.call_number.setText(self.current_entry.call_number)
        self.year.setText(self.current_entry.year)
        self.type.setText(self.current_entry.type)
        self.provenance.setText(self.current_entry.provenance)
        if (self.mms is not None):
            self.mms.setText(self.current_entry.mms_id)
        if self.Suppressed is not None:
            self.Suppressed.setText(self.current_entry.Suppressed)
        if self.other_libraries is not None:
            self.other_libraries.setText(self.current_entry.OtherLibraryInformation)


    def refresh(self):
        if self.current_entry:
            self.title.setText(self.current_entry.name)
            self.author.setText(self.current_entry.author)
            self.location.setText(self.current_entry.location)
            self.year.setText(self.current_entry.year)
            self.type.setText(self.current_entry.type)
            self.provenance.setText(self.current_entry.provenance)
            if (self.mms is not None):
                self.mms.setText(self.current_entry.mms_id)
            if self.Supressed is not None:
                self.Supressed.setText(self.current_entry.Suppressed)
