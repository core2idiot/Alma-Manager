from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from MainWindow.ContentView import ContentView
from MainWindow.ActionPanel import ActionPanel
from Administration.get_logger import get_logger
#   PanelView is the parent class to the two panels that are where Item information Lives
#   And where the actions are selected
class PanelView(QtWidgets.QWidget):
    def __init__(self):
        try:
            #SearchSlot - ContentView
            #self.ActionHandler.ActionDone - Action Panel
            super(PanelView, self).__init__()
            self.layout = QtWidgets.QHBoxLayout()
            self.Content = ContentView()
            self.layout.addWidget(self.Content)
            self.actions = ActionPanel()
            self.Content.NewItem.connect(self.actions.NewItem)
            self.actions.SizeChanged.connect(self.CalculateAndSetSize)
            self.actions.ActionHandler.ActionDone.connect(self.Content.SearchSlot)
            self.scroll = QtWidgets.QScrollArea(self)
            self.scroll.horizontalScrollBar().setEnabled(False)
            self.scroll.setWidget(self.actions)
            self.scroll.setWidgetResizable(True)
            self.scroll.setMinimumSize(self.actions.width() + 35, self.scroll.height())
            self.Content.setMinimumSize(self.actions.width() + 35, self.Content.height())
            self.layout.addWidget(self.scroll)
            self.layout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
            self.setLayout(self.layout)
            get_logger.instance.write_to_log('Created PanelView', 7)

        except Exception as e:
            get_logger.instance.write_to_log('Error rendering PanelView: ' + str(e), 5)


    def CalculateAndSetSize(self, NewActionSize):
        self.scroll.setMinimumSize(NewActionSize, self.scroll.height())
        self.Content.setMinimumSize(NewActionSize, self.scroll.height())


