from PyQt5 import QtWidgets
#from PyQt5.QtGui import *
from MainWindow.DisplayedObject import DisplayedObject
from Administration.get_logger import get_logger
import sys
import os
from PyQt5.QtCore import pyqtSignal
from Types.Item import Item
from API.ItemFetcherThread import ItemFetcherThread
from MainWindow.StatusBarSingleton import StatusBarSingleton
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
#   ContentView contains the searchBar and the left side of mainWindow
class SelectAllLineEdit(QtWidgets.QLineEdit):
    def __init__(self, parent=None):
        super(SelectAllLineEdit, self).__init__(parent)

    def mousePressEvent(self, e):
        self.selectAll()
"""
ContentView
contains the class searchBar and the class DisplayedObject
Designed to be a member of a window
"""
class ContentView(QtWidgets.QWidget):
    NewItem = pyqtSignal(Item)
    def __init__(self):
        try:
            super(ContentView, self).__init__()
            self.vlayout = QtWidgets.QVBoxLayout()
            self.hlayout = QtWidgets.QHBoxLayout()
            self.searchBar = SelectAllLineEdit()
            #self.searchBar.editingFinished.connect(self.UpdateItem)
            self.hlayout.addWidget(self.searchBar)
            self.searchButton = QtWidgets.QPushButton("Search")
            self.searchButton.clicked.connect(self.SearchSlot)
            self.searchBar.returnPressed.connect(self.SearchSlot)
            self.resizeEvent = self.ResizeEvent
            self.hlayout.addWidget(self.searchButton)
            self.hlayout.addStretch()
            self.current_entry = DisplayedObject()
            self.vlayout.addLayout(self.hlayout)
            self.vlayout.addWidget(self.current_entry)
            self.hlayout.addStretch()
            self.setLayout(self.vlayout)
            self.ItemFetcher = ItemFetcherThread()
            self.ItemFetcher.ItemAvail.connect(self.UpdateItem)
            self.ItemFetcher.ItemAvail.connect(self.current_entry.ShowObject)
            get_logger.instance.write_to_log("Rendered ContentView", 7)

        except Exception as e:
            get_logger.instance.write_to_log('Error Rendering ContentView: ' + str(e), 5)

    def ResizeEvent(self, event):
        self.searchBar.setMinimumSize(self.width() - self.searchButton.width() - 20, self.searchButton.height())
        self.resizeEvent = None

    def UpdateItem(self, item):
        try:
            self.NewItem.emit(item)
            if not AlmaManagerConfiguration.instance.MultipleItemMode:
                StatusBarSingleton.instance.showMessage("Found Item: " + item.name)
            super(ContentView, self).activateWindow()
            self.searchBar.setFocus()
            self.searchBar.selectAll()
#           self.write_to_log('Updated DisplayedItem ' + self.current_entry.title.text(), 7)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log('Error Fetching Data, Barcode: '+ self.searchBar.text() + ' Error: ' + str(e), 3)
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                              + str(exc_tb.tb_lineno), 5)
            StatusBarSingleton.instance.showMessage("ERROR: Action/Item Error", 30000)

    def SearchSlot(self):
        try:
            SearchTerm = self.searchBar.text()
            SearchTerm = SearchTerm.strip()
            StatusBarSingleton.instance.showMessage("Searching for " + SearchTerm)
            self.ItemFetcher.SetBarcode(SearchTerm)
            self.ItemFetcher.start()

            #if self.parent_window.continous:
            #    self.parent_window.Panel_View.actions.doContAction()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            StatusBarSingleton.instance.showMessage("ERROR: Item not Found" + str(e), 30000)
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log('Error Fetching Data, Barcode: '+ self.searchBar.text() + ' Error: ' + str(e), 3)
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                              + str(exc_tb.tb_lineno), 5)
