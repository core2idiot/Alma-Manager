#!/usr/bin/env python3
import sys
from PyQt5.QtWidgets import QApplication
from MainWindow.mainWindow import mainWindow

__version__ = "0.07.3"

if __name__ == '__main__':
    APPLICATION = QApplication(["Alma Manager"])
    APPLICATION.setApplicationDisplayName("Alma Manager")
    APPLICATION.setOrganizationDomain("Oregon Institute of Technology")
    MAIN_WINDOW = mainWindow()
    sys.exit(APPLICATION.exec_())
