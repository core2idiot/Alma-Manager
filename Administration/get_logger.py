import time
import sys
from PyQt5.QtWidgets import QWidget
from PyQt5 import QtCore
from Administration.errorWindow import ErrorWindow

class get_logger:
    class __log(QWidget):
        def __init__(self, log_location, log_level, print_log):
            self.log_location = log_location
            self.log_level = log_level
            self.print_log = print_log
            self.error_window = None
            self.mutex = QtCore.QMutex()

        def write_to_log(self, msg, level):
            self.mutex.lock()
            log_time = time.strftime("%x") + ' ' + time.strftime("%X") + ' '
            log_msg = log_time + msg + '\n'
            if self.log_level >= level:
                log = open(self.log_location, "a+")
                log.write(log_msg)
                log.close()
            if self.print_log is True and self.log_level >= level:
                print(log_msg[:-1])
            if level <= 2:
                self.error_window = ErrorWindow(self, msg)
            if level == 0:
                sys.exit()

            self.mutex.unlock()
    instance = None

    def __init__(self, logfile, loglevel, PrintLog):
        if not get_logger.instance:
            get_logger.instance = get_logger.__log(logfile, loglevel, PrintLog)
        else:
            raise Exception("Cannot Create Object if Object exists")

