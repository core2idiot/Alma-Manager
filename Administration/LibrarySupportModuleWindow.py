from PyQt5 import QtWidgets
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Administration.AlmaManager import ManagerConfiguration
from Types.LibrarySupportPackage import LibrarySupportPackage
from API.alma import alma
import sys
import os
import shutil
import importlib
import webbrowser

#   This Class is a window for selecting a library,
#   it should only exist within a settingsWindow

class LibrarySupportModuleWindow(QtWidgets.QWidget):
    def __init__(self):
        super(LibrarySupportModuleWindow, self).__init__()
        self.AddLSMLayout = QtWidgets.QHBoxLayout()
        self.AddLSMLayout.addWidget(QtWidgets.QLabel("Add LSM via Git: "))
        self.NewLSMGit = QtWidgets.QLineEdit()
        self.NewLSMGit.setPlaceholderText("URL")
        self.NewLSMBranch = QtWidgets.QLineEdit()
        self.NewLSMBranch.setPlaceholderText("Branch")
        self.AddLSMLayout.addWidget(self.NewLSMGit)
        self.GitBranchSeparator = QtWidgets.QLabel(" : ")
        self.AddLSMLayout.addWidget(self.GitBranchSeparator)
        self.AddLSMLayout.addWidget(self.NewLSMBranch)
        self.NewLSMGitSubmit = QtWidgets.QPushButton("Submit")
        self.NewLSMGitSubmit.clicked.connect(self.AddLSM)
        self.AddLSMLayout.addWidget(self.NewLSMGitSubmit)
        self.MainLabel = QtWidgets.QLabel("Currently Enabled Library Support Modules")
        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)
        self.layout.addLayout(self.AddLSMLayout)
        self.layout.addWidget(self.MainLabel)
        self.BuildListOfLSMs()
        try:
            import git
        except:
            self.NewLSMGitSubmit.setEnabled(False)
            self.NewLSMBranch.setReadOnly(True)
            self.NewLSMGit.setReadOnly(True)
            for each in self.LSMs:
                each.menu().actions()[1].setEnabled(False)
            get_logger.instance.write_to_log("Unable to import git, some features will be unavailable", 1)
        get_logger.instance.write_to_log("Managing Library Support Modules", 7)

    def BuildListOfLSMs(self):
        self.LSMs = []
        for each in AlmaManagerConfiguration.instance.LibrarySupportPackages:
            self.LSMs.append(QtWidgets.QPushButton(each))
            menu = QtWidgets.QMenu(self.LSMs[-1])
            menu.addAction("View Website", self.ViewWebsite)
            menu.addAction("Check For Updates", self.CheckLSM)
            menu.addAction("Remove Library Suppport Module", self.RemoveLSM)
            self.LSMs[-1].setMenu(menu)
        for each in self.LSMs:
            self.layout.addWidget(each)

    def AddLSM(self):
        try:
            import git
        except Exception as e:
            get_logger.instance.write_to_log("error importing git", 5)
            return
        try:
            downloadFolder = self.NewLSMGit.text().split("/")[-1]
            downloadFolder = downloadFolder.split(".")[0]
            #ToDownload = git.Git("./").clone(self.NewLSMGit.text())
            if self.NewLSMBranch.text() == "":
                get_logger.instance.write_to_log("Adding: " + downloadFolder, 1)
                git.Git("./").clone(self.NewLSMGit.text())
            else:
                get_logger.instance.write_to_log("Adding: " + downloadFolder, 1)
                git.Git("./").clone(self.NewLSMGit.text(), branch=self.NewLSMBranch.text())
            curr = importlib.import_module(downloadFolder + ".__init__")
            temp = getattr(curr, "__name__")
            curr = importlib.import_module(downloadFolder + "." + temp)
            temp = getattr(curr, temp)
            if issubclass(temp, LibrarySupportPackage):
                ManagerConfiguration.AppendOptionInFile("INCLUDE_LIBRARY_SUPPORT_PACKAGE = ", downloadFolder)
                get_logger.instance.write_to_log("Added " + downloadFolder + " to advanced menu", 7)
            else:
                raise Exception("Attempted to import class that wasn't library support module")
            get_logger.instance.write_to_log("Please Restart AlmaManager to access new LSM:" + self.NewLSMGit.text(), 1)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log("Error adding LSM: " + downloadFolder + " : " + str(e), 1)
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                                             + str(exc_tb.tb_lineno), 5)



    def RemoveLSM(self):
        LSMtoRemove = self.sender().parent().parent().text()
        shutil.rmtree("./" + LSMtoRemove)
        ManagerConfiguration.RemoveOptionInFile("INCLUDE_LIBRARY_SUPPORT_PACKAGE = ", LSMtoRemove)
        get_logger.instance.write_to_log("Please Restart AlmaManager to Remove LSM: " + LSMtoRemove , 1)

    def CheckLSM(self):
        try:
            import git
        except Exception:
            get_logger.instance.write_to_log("Error importing git", 1)
            return
        LSMtoCheck = self.sender().parent().parent().text()
        check = git.cmd.Git(LSMtoCheck)
        get_logger.instance.write_to_log(LSMtoCheck + ": " + check.pull(), 1)

    def ViewWebsite(self):
        CurrentLSM = self.sender().parent().parent().text()
        curr = importlib.import_module(CurrentLSM + ".__init__")
        temp = getattr(curr, "__url__")
        webbrowser.open(temp)
        get_logger.instance.write_to_log("Opened Website", 7)

