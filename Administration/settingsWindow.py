from PyQt5 import QtWidgets
from Administration.ManageActionList import ManageActionList
from Administration.LibrarySelectionWindow import SelectALibrary
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Administration.AlmaManager import ManagerConfiguration
from Administration.LibrarySupportModuleWindow import LibrarySupportModuleWindow
import sys
import os
import webbrowser

class SettingsWindow(QtWidgets.QWidget):
    def __init__(self):
        try:
            super(SettingsWindow, self).__init__()
#           Copy parent data to local space so that we don't
#           have to pass it in each time and can change data contained within
#           AlmaConfiguration
            self.setWindowTitle("Settings")
            self.layout = QtWidgets.QGridLayout()
#           Alma URL Information
            self.AlmaURLLabel = QtWidgets.QLabel("Alma URL: ")
            self.layout.addWidget(self.AlmaURLLabel, 0, 0)
            self.AlmaURL = QtWidgets.QLineEdit()
            self.AlmaURL.setText(AlmaManagerConfiguration.instance.url)
            self.layout.addWidget(self.AlmaURL, 0, 1)
#           API Key information
            self.API_KEYLabel = QtWidgets.QLabel("Alma API Key: ")
            self.layout.addWidget(self.API_KEYLabel, 1, 0)
            self.API_KEY = QtWidgets.QLineEdit()
            self.API_KEY.setText(AlmaManagerConfiguration.instance.ApiKey)
            self.layout.addWidget(self.API_KEY, 1, 1)
#           SRU URL Settings
            self.SRU_URLLabel = QtWidgets.QLabel("SRU URL: ")
            self.layout.addWidget(self.SRU_URLLabel, 2, 0)
            self.SRU_URL = QtWidgets.QLineEdit()
            self.SRU_URL.setText(AlmaManagerConfiguration.instance.SRU_URL)
            self.layout.addWidget(self.SRU_URL, 2, 1)
#           SRU Alliance Settings
            self.SRUAllianceLabel = QtWidgets.QLabel("SRU Alliance: ")
            self.layout.addWidget(self.SRUAllianceLabel, 3, 0)
            self.SRUAlliance = QtWidgets.QLineEdit()
            self.SRUAlliance.setText(AlmaManagerConfiguration.instance.SRU_Alliance)
            self.layout.addWidget(self.SRUAlliance, 3, 1)
#           SRU Institution Settings
            self.SRUInstitutionLabel = QtWidgets.QLabel("SRU Instituion: ")
            self.layout.addWidget(self.SRUInstitutionLabel, 4, 0)
            self.SRUInstitution = QtWidgets.QLineEdit()
            self.SRUInstitution.setText(AlmaManagerConfiguration.instance.SRU_Institution)
            self.layout.addWidget(self.SRUInstitution, 4, 1)
            self.SRU_Enable = QtWidgets.QCheckBox("Use Alma SRU to get holdings from other libraries")
            self.SRU_Enable.setChecked(AlmaManagerConfiguration.instance.Enable_SRU)
            self.layout.addWidget(self.SRU_Enable, 9, 0)

            self.MultipleItemConfirm = QtWidgets.QCheckBox("Confirm item prior to performing action in multiple item mode")
            self.MultipleItemConfirm.setChecked(AlmaManagerConfiguration.instance.ConfirmItemMultipleItem)
#           Api provenanceCodes
            self.apiProvenances = QtWidgets.QCheckBox("Use the Alma API to get Provenance Codes")
            if AlmaManagerConfiguration.instance.API_PROVENANCES:
                self.apiProvenances.setChecked(True)
#           Api Locations
            self.apiLocations = QtWidgets.QCheckBox("Use the Alma API to get Permanent Locations")
            if AlmaManagerConfiguration.instance.API_LOCATIONS:
                self.apiLocations.setChecked(True)
            self.EnableBibSuppression = QtWidgets.QCheckBox("Enable Bib Suppression")
            if AlmaManagerConfiguration.instance.Enable_Suppress:
                self.EnableBibSuppression.setChecked(True)
#           Log Echoing
            self.ECHO_LOG = QtWidgets.QCheckBox("Echo Log Events to Terminal")
            if AlmaManagerConfiguration.instance.printLogMsg is True:
                self.ECHO_LOG.setChecked(True)
#           Saving Settings
            self.apply = QtWidgets.QPushButton("Apply")
            self.apply.clicked.connect(self.ApplyAllSettings)
            self.save = QtWidgets.QPushButton("Save Settings")
            self.save.clicked.connect(self.SaveAllSettings)
#           Select A Library
            if AlmaManagerConfiguration.instance.default_library is None:
                self.currentLibrary = QtWidgets.QLabel("Current Library: N/A")
            else:
                self.currentLibrary = QtWidgets.QLabel("Current Library: " + AlmaManagerConfiguration.instance.default_library)
            self.selectLib = QtWidgets.QPushButton("Select a Library")
            self.selectLib.clicked.connect(self.SelectLibrary)
#           Open Alma Manager URL
            self.AlmaManagerURL = QtWidgets.QPushButton("Open Alma Manager Website")
            self.AlmaManagerURL.clicked.connect(lambda: webbrowser.open('https://gitlab.com/core2idiot/Alma-Manager'))
#           Import Version Number
            from __init__ import __version__
#           Create Object to display version number
            self.AlmaManagerVersion = QtWidgets.QLabel("Alma Manager Version: " + __version__)
#           Adding all the widgets to the SettingsWindow
#            self.layout.addWidget(self.AlmaURL)
            self.ManageActions = QtWidgets.QPushButton("Manage Action List")
            self.ManageActions.clicked.connect(self.ManageActionsFunc)
            self.ManageLibSupport = QtWidgets.QPushButton("Manage Library Support Modules")
            self.ManageLibSupport.clicked.connect(self.ManageLibrarySupportFunc)
            self.OpenLog = QtWidgets.QPushButton("Open Alma Manager Log")
            self.OpenLog.clicked.connect(lambda: webbrowser.open('alma.log'))
            self.layout.addWidget(self.selectLib, 5, 1)
            self.layout.addWidget(self.currentLibrary, 5, 0)
            self.layout.addWidget(self.ECHO_LOG, 6, 0)
            self.layout.addWidget(self.apiProvenances, 7, 0)
            self.layout.addWidget(self.apiLocations, 8, 0)
            self.layout.addWidget(self.EnableBibSuppression, 10, 0)
            self.layout.addWidget(self.MultipleItemConfirm, 11, 0)
            self.layout.addWidget(self.ManageActions, 12, 0)
            self.layout.addWidget(self.ManageLibSupport, 12, 1)
            self.apply.setEnabled(False)
            #self.layout.addWidget(self.apply, 15, 0)
            self.layout.addWidget(self.save, 15, 1)
            self.layout.addWidget(self.OpenLog, 13, 1)
            self.layout.addWidget(self.AlmaManagerURL, 13, 0)
            self.layout.addWidget(self.AlmaManagerVersion, 14, 0)
            self.layout.setColumnMinimumWidth(0, 350)
            self.layout.setColumnMinimumWidth(1, 350)
            self.layout.setColumnStretch(1, 350)
            self.layout.setColumnStretch(0, 350)
            self.setLayout(self.layout)
            self.show()
            get_logger.instance.write_to_log("Created SettingsWindow", 7)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            get_logger.instance.write_to_log("Error Creating SettingsWindow: " + str(e), 1)
            get_logger.instance.write_to_log(str(exc_type) + ' ' + fname + ':'
                                             + str(exc_tb.tb_lineno), 5)

    def SaveAllSettings(self):
        try:
            self.ApplyAllSettings()
            ManagerConfiguration.write_config()
            get_logger.instance.write_to_log("Please restart Alma Manager to refresh settings", 2)
            get_logger.instance.write_to_log("Successfully wrote to AlmaManager.conf", 10)
        except Exception as e:
            get_logger.instance.write_to_log("ERROR: SettingsWindow: Unable to Save Changes: " + str(e), 2)

    def ThrowError(self):
        get_logger.instance.write_to_log("This is an Error", 1)

    def ManageActionsFunc(self):
        get_logger.instance.write_to_log("Opening the manage action list window", 7)
        self.ManageActionsWindow = ManageActionList(self)

    def ManageLibrarySupportFunc(self):
        self.ManageLibSupportWindow = LibrarySupportModuleWindow()
        self.ManageLibSupportWindow.show()

    def ApplyAllSettings(self):
        try:
            AlmaManagerConfiguration.instance.ApiKey = self.API_KEY.text()
            AlmaManagerConfiguration.instance.printLogMsg = self.ECHO_LOG.isChecked()
            AlmaManagerConfiguration.instance.url = self.AlmaURL.text()
            AlmaManagerConfiguration.instance.API_PROVENANCES = self.apiProvenances.isChecked()
            AlmaManagerConfiguration.instance.API_LOCATIONS = self.apiLocations.isChecked()
            AlmaManagerConfiguration.instance.SRU_URL = self.SRU_URL.text()
            AlmaManagerConfiguration.instance.SRU_Alliance = self.SRUAlliance.text()
            AlmaManagerConfiguration.instance.SRU_Institution = self.SRUInstitution.text()
            AlmaManagerConfiguration.instance.Enable_SRU = self.SRU_Enable.isChecked()
            AlmaManagerConfiguration.instance.Enable_Suppress = self.EnableBibSuppression.isChecked()
            AlmaManagerConfiguration.instance.ConfirmItemMultipleItem = self.MultipleItemConfirm.isChecked()

        except Exception as e:
            get_logger.instance.write_to_log("ERROR: SettingsWindow:  Unable to Apply Changes: " + str(e), 2)

    def SelectLibrary(self):
        self.library_selection_window = SelectALibrary(self)


