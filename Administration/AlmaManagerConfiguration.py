from PyQt5.QtCore import QMutex
from Administration.get_logger import get_logger
class AlmaManagerConfiguration:
    class __Configuration:
        def __init__(self):
            self.__mutex = QMutex()
            self.ActionList = []
            self.KnownActions = []
            self.HiddenActions = []
            self.LibrarySupportPackages = []
            self.ApiKey = None
            self.url = None
            self.default_library = None
            self.printLogMsg = True
            self.API_PROVENANCES = True
            self.API_LOCATIONS = True
            self.Display_MMS_ID = False
            self.Enable_Suppress = False
            self.Disable_Settings = False
            self.Enable_SRU = False
            self.SRU_Institution = None
            self.SRU_Alliance = None
            self.SRU_URL = None
            self.LogLevel = 10
            self.MultipleItemMode = False
            self.MultipleItemModeEnabled = True
            self.ConfirmItemMultipleItem = True
            self.DebugMode = False


        def __setattr__(self, attr, value):
            #print("attr: " + str(attr) + " value:" + str(value))
            try:
                self.__mutex.lock()
                super().__setattr__(attr, value)
                self.__mutex.unlock()
            except Exception:
                super().__setattr__(attr, value)
#       Check to make sure invalid settings aren't set
        def validate_settings(self):
#       if SRU is enabled we then need an institution, alliance and url
            try:
                if self.Enable_SRU is True:
                    if self.SRU_URL is None or self.SRU_Institution is None or self.SRU_Alliance is None:
                        raise Exception("SRU Enabled but needed Institution, URL and/or Alliance is missing")
                if self.ApiKey is None:
                    raise Exception("No API Key Specified")
                if self.url is None:
                    raise Exception("No API URL Specified")
                if self.default_library is None:
                    raise Exception("No Default Library Specified")
            except Exception as e:
                get_logger.instance.write_to_log("Error Validating Configuration: " + str(e), 1)
#        def __setattr__(cls, attr, value):
#            raise Exception("Can't change values post init")
    instance = None
    def __init__(self):
        if not AlmaManagerConfiguration.instance:
            AlmaManagerConfiguration.instance = AlmaManagerConfiguration.__Configuration()
        else:
            raise Exception("No need to create an object after initialization")
