import os
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Types.Action import Action
from pathlib import Path
from Types.ActionType import ActionType

class ManagerConfiguration(object):
    """This Class contains the configuration for AlmaManager
    Attributes:
        parseOption:
            This function contains the massive if tree to handle
            each item that is possible with AlmaManager.conf
        write_config:
            This function attempts to generate an AlmaManager.conf
            It
    """
    def __init__(self):
        from __init__ import __version__
        self.version = __version__

        if os.path.exists(str(Path.home()) + '/AlmaManager/AlmaManager.conf'):
            AlmaManagerConfiguration.instance.config_location = str(Path.home()) + '/AlmaManager/AlmaManager.conf'
            AlmaManagerConfiguration.instance.LogFile = str(Path.home()) + '/AlmaManager/alma.log'
            get_logger(AlmaManagerConfiguration.instance.LogFile, AlmaManagerConfiguration.instance.LogLevel, AlmaManagerConfiguration.instance.printLogMsg)
            get_logger.instance.write_to_log("---Using User Specific Configuration File---", 4)
            self.init_existing_config()
        elif os.path.exists('./AlmaManager.conf'):
            AlmaManagerConfiguration.instance.config_location = './AlmaManager.conf'
            self.init_existing_config()
            get_logger(AlmaManagerConfiguration.instance.LogFile, AlmaManagerConfiguration.instance.LogLevel, AlmaManagerConfiguration.instance.printLogMsg)
            get_logger.instance.write_to_log("---Using Global Configuration File---", 4)
        else:
            AlmaManagerConfiguration.instance.config_location ='./AlmaManager.conf'
            AlmaManagerConfiguration.instance.LogFile = "alma.log"
            AlmaManagerConfiguration.instance.LogLevel = 10
            get_logger(AlmaManagerConfiguration.instance.LogFile, AlmaManagerConfiguration.instance.LogLevel, AlmaManagerConfiguration.instance.printLogMsg)
            get_logger.instance.write_to_log("AlmaManager.conf not found", 1)
        get_logger.instance.write_to_log('LOG_LEVEL: ' + str(AlmaManagerConfiguration.instance.LogLevel)
                          + ' API_KEY=' + str(AlmaManagerConfiguration.instance.ApiKey)
                          + ' VERSION=' + self.version
                          + " LOG_FILE=" + str(AlmaManagerConfiguration.instance.LogFile), 4)


    def init_existing_config(self):
        configFile = open (AlmaManagerConfiguration.instance.config_location)
        for line in configFile:
            line = line.strip();
            if not line.startswith("#") and line != "":
                self.parse_option(line)

#   This function either modifies an existing AlmaManager.conf or
#   generates a new one depending on if the file exists.
    @staticmethod
    def write_config():
        if not os.path.exists(AlmaManagerConfiguration.instance.config_location):
            with open(AlmaManagerConfiguration.instance.config_location, 'w') as fp:
                pass
            ManagerConfiguration.update_existing_config()
        else:
            ManagerConfiguration.update_existing_config()

    @staticmethod
    def update_existing_config():
        ManagerConfiguration.ChangeOptionInFile("API_KEY = ", AlmaManagerConfiguration.instance.ApiKey)
        if AlmaManagerConfiguration.instance.printLogMsg:
            ManagerConfiguration.ChangeOptionInFile("PRINT_LOG_MSG = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("PRINT_LOG_MSG = ", "no")

        if AlmaManagerConfiguration.instance.API_PROVENANCES:
            ManagerConfiguration.ChangeOptionInFile("USE_API_FOR_PROVENANCE_CODES = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("USE_API_FOR_PROVENANCE_CODES = ", "no")

        if AlmaManagerConfiguration.instance.API_LOCATIONS:
            ManagerConfiguration.ChangeOptionInFile("USE_API_FOR_PERMANENT_LOCATIONS = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("USE_API_FOR_PERMANENT_LOCATIONS = ", "no")

        if AlmaManagerConfiguration.instance.SRU_Alliance != "":
            ManagerConfiguration.ChangeOptionInFile("SRU_ALLIANCE = ", AlmaManagerConfiguration.instance.SRU_Alliance)

        if AlmaManagerConfiguration.instance.SRU_Institution != "":
            ManagerConfiguration.ChangeOptionInFile("SRU_INSTITUTION = ", AlmaManagerConfiguration.instance.SRU_Institution)

        if AlmaManagerConfiguration.instance.SRU_URL != "":
            ManagerConfiguration.ChangeOptionInFile("SRU_URL = ", AlmaManagerConfiguration.instance.SRU_URL)

        if AlmaManagerConfiguration.instance.Enable_SRU:
            ManagerConfiguration.ChangeOptionInFile("ENABLE_SRU = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("ENABLE_SRU = ", "no")

        if AlmaManagerConfiguration.instance.url is not None:
            ManagerConfiguration.ChangeOptionInFile("ALMA_URL = ", AlmaManagerConfiguration.instance.url)

        if AlmaManagerConfiguration.instance.default_library is not None:
            ManagerConfiguration.ChangeOptionInFile("DEFAULT_LIBRARY = ", AlmaManagerConfiguration.instance.default_library)

        if AlmaManagerConfiguration.instance.Enable_Suppress:
            ManagerConfiguration.ChangeOptionInFile("ENABLE_BIB_SUPPRESSION = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("ENABLE_BIB_SUPPRESSION = ", "no")

        if AlmaManagerConfiguration.instance.ConfirmItemMultipleItem:
            ManagerConfiguration.ChangeOptionInFile("CONFIRM_ACTION_IN_MULTIPLE_ITEM_MODE = ", "yes")
        else:
            ManagerConfiguration.ChangeOptionInFile("CONFIRM_ACTION_IN_MULTIPLE_ITEM_MODE = ", "no")

        ManagerConfiguration.ChangeOptionInFile("LOG_FILE = ", AlmaManagerConfiguration.instance.LogFile)
        ManagerConfiguration.ChangeOptionInFile("LOG_LEVEL = ", str(AlmaManagerConfiguration.instance.LogLevel))

#   This parses on line in AlmaManager.conf, allowing the configuration file
#   to be in any order they decide.
    def parse_option(self, line):
        segments = line.split(" ")
        if line.startswith("PERMISSION_LEVEL = "):
            get_logger.instance.write_to_log("Permission Level is no longer supported, "
                               + "Please Remove it from your AlmaManager.conf", 5)
        elif line.startswith("ALMA_URL = "):
            AlmaManagerConfiguration.instance.url = str(segments[2])
        elif line.startswith("LOG_LEVEL = "):
            AlmaManagerConfiguration.instance.LogLevel = int(segments[2])
        elif line.startswith("API_KEY = "):
            AlmaManagerConfiguration.instance.ApiKey = segments[2]
        elif line.startswith("PRINT_LOG_MSG = "):
            if segments[2] == 'no':
                AlmaManagerConfiguration.instance.printLogMsg = False
        elif line.startswith("USE_API_FOR_PROVENANCE_CODES = "):
            if segments[2] == 'no':
                AlmaManagerConfiguration.instance.API_PROVENANCES = False
        elif line.startswith("USE_API_FOR_PERMANENT_LOCATIONS = "):
            if segments[2] == 'no':
                AlmaManagerConfiguration.instance.API_LOCATIONS = False
        elif line.startswith("DISPLAY_MMS_ID = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.Display_MMS_ID = True
        elif line.startswith("ENABLE_BIB_SUPPRESSION = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.Enable_Suppress = True
        elif line.startswith("DISABLE_SETTINGS_MENU = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.Disable_Settings = True
        elif line.startswith("ENABLE_DEBUG = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.DebugMode = True
        elif line.startswith("CONFIRM_ACTION_IN_MULTIPLE_ITEM_MODE = "):
            if segments[2] == 'no':
                AlmaManagerConfiguration.instance.ConfirmItemMultipleItem = False
        elif line.startswith("LOG_FILE = "):
            AlmaManagerConfiguration.instance.LogFile = segments[2]
        elif line.startswith("DEFAULT_LIBRARY = "):
            AlmaManagerConfiguration.instance.default_library = segments[2]
        elif line.startswith("KNOWN_LOCATION = "):
            AlmaManagerConfiguration.instance.KnownActions.append(Action(ActionType.PERMANENT_LOCATION, line.replace("KNOWN_LOCATION = ", "").strip()))
        elif line.startswith("KNOWN_PROVENANCE_CODE = "):
            AlmaManagerConfiguration.instance.KnownActions.append(Action(ActionType.PROVENANCE_CODE, line.replace("KNOWN_PROVENANCE_CODE = ", "").strip()))
        elif line.startswith("HIDE_LOCATION = "):
            AlmaManagerConfiguration.instance.HiddenActions.append(Action(ActionType.PERMANENT_LOCATION, line.replace("HIDE_LOCATION = ", "").strip()))
        elif line.startswith("HIDE_PROVENANCE_CODE = "):
            AlmaManagerConfiguration.instance.HiddenActions.append(Action(ActionType.PROVENANCE_CODE, line.replace("HIDE_PROVENANCE_CODE = ", "").strip()))
        elif line.startswith("INCLUDE_LIBRARY_SUPPORT_PACKAGE = "):
            AlmaManagerConfiguration.instance.LibrarySupportPackages.append(line.replace("INCLUDE_LIBRARY_SUPPORT_PACKAGE = ", ""))
        elif line.startswith("ENABLE_SRU = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.Enable_SRU = True
        elif line.startswith("SRU_ALLIANCE = "):
            AlmaManagerConfiguration.instance.SRU_Alliance = line.replace("SRU_ALLIANCE = ", "")
        elif line.startswith("SRU_INSTITUTION = "):
            AlmaManagerConfiguration.instance.SRU_Institution = line.replace("SRU_INSTITUTION = ", "")
        elif line.startswith("SRU_URL = "):
            AlmaManagerConfiguration.instance.SRU_URL = line.replace("SRU_URL = ", "")
        elif line.startswith("DISABLE_MULTIPLE_ITEM_MODE = "):
            if segments[2] == 'yes':
                AlmaManagerConfiguration.instance.MultipleItemModeEnabled = False
        elif line.isspace():
            pass
        else:
            print("Invalid Option " + line)

#   This function reads the entire file and then rewrites the file replacing
#   a single line.  This is a less than ideal implementation, but is cross platform.
    @staticmethod
    def ChangeOptionInFile(CurrentOption, NewOptionValue):
        retVal = False
        with open (AlmaManagerConfiguration.instance.config_location, "r") as file:
            lines = file.readlines()
        with open(AlmaManagerConfiguration.instance.config_location, "w") as file:
            for line in lines:
                if not line.strip("\n").startswith(CurrentOption):
                    file.write(line)
                else:
                    file.write(CurrentOption + NewOptionValue + "\n")
                    retVal = True
            if not retVal:
                file.write(CurrentOption + NewOptionValue + "\n")
        return retVal

    @staticmethod
    def AppendOptionInFile(CurrentOption, OptionValue):
        with open(AlmaManagerConfiguration.instance.config_location, "a") as file:
            file.write(CurrentOption + OptionValue + "\n")

    @staticmethod
    def RemoveOptionInFile(CurrentOption, OptionValue):
        with open (AlmaManagerConfiguration.instance.config_location, "r") as file:
            lines = file.readlines()
        with open(AlmaManagerConfiguration.instance.config_location, "w") as file:
            for line in lines:
                if not line.startswith(CurrentOption + OptionValue):
                    file.write(line)
#   This function is used widely in AlmaManager in order to enable consistent
#   logging.  It will only log things to file if they are less than the maximum
#   log level, and if a logged element is less than 2, it will open a window
#   to notify the user of the the logged message.  If a logged message's level is
#   0 it is assumed this is a fatal error, and we should close the application.

