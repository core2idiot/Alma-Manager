from PyQt5 import QtWidgets
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from API.alma import alma
#   This Class is a window for selecting a library,
#   it should only exist within a settingsWindow
class SelectALibrary(QtWidgets.QWidget):
    def __init__(self, window):
        super(SelectALibrary, self).__init__()
        self.parent_window = window
        self.library_widgets = []

        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setFixedSize(435, 600)

        self.inner = QtWidgets.QFrame(self.scroll)
        self.inner.setLayout(QtWidgets.QVBoxLayout())

        self.scroll.setWidget(self.inner)
        try:
#           def GetLibraries(url, APIKey):
            self.libraries = alma.GetLibraries(AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
            for each in self.libraries:
                self.library_widgets.append(QtWidgets.QPushButton(each[0].text))
                self.library_widgets[-1].setFixedSize(400, 30)
                self.library_widgets[-1].clicked.connect(self.SelectLib)
                self.inner.layout().addWidget(self.library_widgets[-1])
            self.scroll.setWindowTitle("Select a Library")
            self.scroll.show()
            get_logger.instance.write_to_log("Displayed Library selection window", 7)
        except Exception as e:
            get_logger.instance.write_to_log("ERROR: Library Selection Window: " + str(e), 1)
            self.close()

    def SelectLib(self):
        NewLibrary = self.sender()
        AlmaManagerConfiguration.instance.default_library = NewLibrary.text()
        self.parent_window.currentLibrary.setText("Current Library: " + NewLibrary.text())
        get_logger.instance.write_to_log("Wrote " + NewLibrary.text() + " to default_library", 8)
        self.scroll.close()

