from PyQt5 import QtWidgets
from PyQt5 import QtCore


class ErrorWindow(QtWidgets.QWidget):
    def __init__(self, log, msg):
        super(ErrorWindow, self).__init__()
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowTitle("NOTICE")
        self.resize(250, 150)
        self.layout = QtWidgets.QVBoxLayout()
        self.msg = QtWidgets.QLabel(msg)
        self.ok = QtWidgets.QPushButton("Okay")
        self.layout.addWidget(self.msg)
        self.layout.addWidget(self.ok)
        self.ok.clicked.connect(self.close)
        self.setLayout(self.layout)
        self.show()

