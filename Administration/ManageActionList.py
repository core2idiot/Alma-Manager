from PyQt5 import QtWidgets
import PyQt5.QtCore
from Types.ActionType import ActionType
from Administration.get_logger import get_logger
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from API.ActionFetcherThread import ActionFetcherThread
"""
NonScrollingDropDown
Reimplements the wheel event of a QtWidgets.QComboBox as no action
So that the User can scroll down in the containing Widget to select
"""
class NonScrollingDropDown(QtWidgets.QComboBox):
    def __init__(self):
        QtWidgets.QComboBox.__init__(self)
    def wheelEvent(self, ev):
        if ev.type() == PyQt5.QtCore.QEvent.Wheel:
            ev.ignore()
"""
ManageActionList
This class is a Window containing a list of actions, enabling the user to modify the actions
within AlmaManager.conf
class ActionFetcherThread(QThread):
"""
class ManageActionList(QtWidgets.QWidget):
    def __init__(self, parent_window):
        super(ManageActionList, self).__init__()
        self.ActionFetcherThread = ActionFetcherThread()
        self.setWindowTitle("Manage Action List")
        get_logger.instance.write_to_log("Loading Action List Management", 9)
        self.HideAll = QtWidgets.QPushButton("Make All Hidden")
        self.HideAll.clicked.connect(self.MakeAllHiddenFunc)
        self.MakeAllKnown = QtWidgets.QPushButton("Make All Known")
        self.MakeAllKnown.clicked.connect(self.MakeAllKnownFunc)
        self.RelyOnApiForAll = QtWidgets.QPushButton("Make All Rely on API")
        self.RelyOnApiForAll.clicked.connect(self.MakeAllApiFunc)
        self.ActionFetcher = ActionFetcherThread()
        self.ActionFetcher.ActionsAvail.connect(self.PopulateActionList)
        self.ActionFetcher.start()
        self.config_location = AlmaManagerConfiguration.instance.config_location
        self.actionList = []
        self.ProvCodesToHide = []
        self.LocationsToHide = []
        self.ProvCodesToRemember = []
        self.LocationsToRemember = []
        self.ActionsToApi = []
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.HideAll)
        self.layout.addWidget(self.MakeAllKnown)
        self.layout.addWidget(self.RelyOnApiForAll)
        self.scroll = QtWidgets.QScrollArea()
        self.scroll.setWidgetResizable(True)
        self.scroll.setFixedSize(435, 600)

        self.inner = QtWidgets.QFrame(self.scroll)
        self.inner.setLayout(QtWidgets.QVBoxLayout())

        self.scroll.setWidget(self.inner)


        self.applyButt = QtWidgets.QPushButton("Apply")
        self.applyButt.clicked.connect(self.applyChanges)
        self.layout.addWidget(self.scroll)
        self.layout.addWidget(self.applyButt)
        self.setLayout(self.layout)
        self.show()
        get_logger.instance.write_to_log("We have rendered ManageActionList", 9)

    def MakeAllKnownFunc(self):
        for each in self.actionList:
            if each.Action.Type == ActionType.PERMANENT_LOCATION:
                self.LocationsToRemember.append(each.itemText(1))
            elif each.Action.Type == ActionType.PROVENANCE_CODE:
                self.ProvCodesToRemember.append(each.itemText(1))
        get_logger.instance.write_to_log("All Actions Known", 1)

    def MakeAllHiddenFunc(self):
        for each in self.actionList:
            if each.Action.Type == ActionType.PERMANENT_LOCATION:
                self.LocationsToHide.append(each.itemText(1))
            elif each.Action.Type == ActionType.PROVENANCE_CODE:
                self.ProvCodesToHide.append(each.itemText(1))
        get_logger.instance.write_to_log("All Actions Hidden", 1)

    def MakeAllApiFunc(self):
        for each in self.actionList:
            self.ActionsToApi.append(each.itemText(1))
        get_logger.instance.write_to_log("All Actions Api'ed", 1)

    def PopulateActionList(self, Actions):
        for each in Actions:
            self.actionList.append(NonScrollingDropDown())
            self.actionList[-1].addItem(str(each.HumanReadable))
            self.actionList[-1].addItem(str(each.MachineReadable))
            self.actionList[-1].addItem("Rely on API")
            self.actionList[-1].addItem("Make Hidden")
            self.actionList[-1].addItem("Make Known")
            self.actionList[-1].Action = each
            self.actionList[-1].activated.connect(self.addAction)
            self.inner.layout().addWidget(self.actionList[-1])
            get_logger.instance.write_to_log('Added ' + each.HumanReadable + ' to list of actions', 8)
        for each in AlmaManagerConfiguration.instance.KnownActions:
            self.actionList.append(NonScrollingDropDown())
            self.actionList[-1].addItem(str(each.HumanReadable))
            self.actionList[-1].addItem(str(each.MachineReadable))
            self.actionList[-1].addItem("Rely on API")
            self.actionList[-1].addItem("Make Hidden")
            self.actionList[-1].addItem("Make Known")
            self.actionList[-1].Action = each
            self.actionList[-1].activated.connect(self.addAction)
            self.inner.layout().addWidget(self.actionList[-1])
            get_logger.instance.write_to_log('Added ' + each.HumanReadable + ' to list of actions', 8)

    def addAction(self):
        ActionDone = False
        if self.sender().Action.Type == ActionType.PERMANENT_LOCATION:
            if self.sender().itemText(1) in self.LocationsToHide:
                ActionDone = True
                self.LocationsToHide.remove(self.sender().itemText(1))
            elif self.sender().itemText(1) in self.LocationsToRemember:
                ActionDone = True
                self.LocationsToRemember.remove(self.sender().itemText(1))
            elif self.sender().itemText(1) in self.ActionsToApi:
                ActionDone = True
                self.ActionsToApi.remove(self.sender().itemText(1))
        elif self.sender().Action.Type == ActionType.PROVENANCE_CODE:
            if self.sender().itemText(1) in self.ProvCodesToHide:
                ActionDone = True
                self.ProvCodesToHide.remove(self.sender().itemText(1))
            elif self.sender().itemText(1) in self.ProvCodesToRemember:
                ActionDone = True
                self.ProvCodesToRemember.remove(self.sender().itemText(1))
            elif self.sender().itemText(1) in self.ActionsToApi:
                ActionDone = True
                self.ActionsToApi.remove(self.sender().itemText(1))

        if self.sender().currentText() == "Rely on API":
            ActionDone = True
            self.ActionsToApi.append(self.sender().itemText(1))

        elif self.sender().Action.Type == ActionType.PERMANENT_LOCATION:
            if self.sender().currentText() == "Make Hidden":
                ActionDone = True
                self.LocationsToHide.append(self.sender().itemText(1))
            elif self.sender().currentText() == "Make Known":
                ActionDone = True
                self.LocationsToRemember.append(self.sender().itemText(1))

        elif self.sender().Action.Type == ActionType.PROVENANCE_CODE:
            if self.sender().currentText() == "Make Hidden":
                ActionDone = True
                self.ProvCodesToHide.append(self.sender().itemText(1))
            elif self.sender().currentText() == "Make Known":
                ActionDone = True
                self.ProvCodesToRemember.append(self.sender().itemText(1))

        if ActionDone is True:
            get_logger.instance.write_to_log("Added " + self.sender().itemText(0)
                              + " To List of Actions to Manip", 9)

    def applyChanges(self):
        get_logger.instance.write_to_log("Applying Changes", 7)
        self.RemoveKnownOptions(self.ProvCodesToHide)
        self.RemoveKnownOptions(self.ProvCodesToRemember)
        self.RemoveKnownOptions(self.LocationsToHide)
        self.RemoveKnownOptions(self.LocationsToRemember)
        self.RemoveKnownOptions(self.ActionsToApi)
        self.WriteOptions("HIDE_PROVENANCE_CODE = ", self.ProvCodesToHide)
        self.WriteOptions("KNOWN_PROVENANCE_CODE = ", self.ProvCodesToRemember)
        self.WriteOptions("HIDE_LOCATION = ", self.LocationsToHide)
        self.WriteOptions("KNOWN_LOCATION = ", self.LocationsToRemember)
        self.close()
        get_logger.instance.write_to_log("Applied Changes", 7)

    def RemoveKnownOptions(self, option):
        get_logger.instance.write_to_log("Removing Options", 5)
        self.RemoveOptions("HIDE_PROVENANCE_CODE = ", option)
        self.RemoveOptions("KNOWN_PROVENANCE_CODE = ", option)
        self.RemoveOptions("HIDE_LOCATION = ", option)
        self.RemoveOptions("KNOWN_LOCATION = ", option)
        get_logger.instance.write_to_log("Removed Options", 7)


    def RemoveOptions(self, option, actions):
        for each in actions:
            self.RemoveFromFile(option + each)

    def WriteOptions(self, option, actions):
        if actions:
            AlmaManagerConf = open(self.config_location, "a")
            AlmaManagerConf.write("\n###These were automatically Generated###\n")
            for each in actions:
                AlmaManagerConf.write(option + each + "\n")
            AlmaManagerConf.close()
            get_logger.instance.write_to_log("Wrote Options", 6)

    def RemoveFromFile(self, option):
        get_logger.instance.write_to_log("Removing Action: " + option, 7)
        with open (self.config_location, "r") as file:
            lines = file.readlines()
        with open(self.config_location, "w") as file:
            for line in lines:
                if line.strip("\n") != option:
                    file.write(line)
