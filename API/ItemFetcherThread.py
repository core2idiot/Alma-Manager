from API.sru import sru
from API.alma import alma
from PyQt5.QtCore import QThread, pyqtSignal
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Types.Item import Item
class ItemFetcherThread(QThread):
    ItemAvail = pyqtSignal(Item)
    ItemError = pyqtSignal(str)
    def __init__(self):
        super(ItemFetcherThread, self).__init__()
    def run(self):
    #def GetItemDataByBarcode(barcode, url, APIKey):
        try:
            itemData = alma.GetItemDataByBarcode(self.Barcode, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
            item = Item(itemData)
            if AlmaManagerConfiguration.instance.Enable_SRU:
#    def FindCurrentAllianceHoldingsBYIZ_MMSID(IZ_MMSID, IZ_Name, NZ_Name, SRU_URL):
                try:
                    LibraryHolding = sru.FindCurrentAllianceHoldingsBYIZ_MMSID(item.mms_id, AlmaManagerConfiguration.instance.SRU_Institution, AlmaManagerConfiguration.instance.SRU_Alliance, AlmaManagerConfiguration.instance.SRU_URL)
                    item.OtherLibraryInformation = ""
                    for each in LibraryHolding:
                        item.OtherLibraryInformation += ("\n" + each)
                except Exception:
                    item.OtherLibraryInformation = "N/A"
            if AlmaManagerConfiguration.instance.Enable_Suppress:
                try:
                    Bib = alma.GetBib(item.mms_id, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                    item.Suppressed = 'Suppression: ' + Bib.find('suppress_from_publishing').text
                except Exception:
                    item.Suppressed = "N/A"
            self.ItemAvail.emit(item)
        except Exception as e:
            self.ItemError.emit(str(e))


    def SetBarcode(self, Barcode):
        self.Barcode = Barcode
