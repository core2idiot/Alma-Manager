from API.alma import alma
from PyQt5.QtCore import QThread, pyqtSignal
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Types.Action import Action
from Types.ActionType import ActionType
class ActionFetcherThread(QThread):
    ActionsAvail = pyqtSignal(list)
    ActionsError = pyqtSignal(str)
    def __init__(self):
        super(ActionFetcherThread, self).__init__()
    def run(self):
        self.GetListofActions()
#   Utilizing self.GetPermanentLocations() and self.GetProvCodes, this function
#   builds a list of actions within self.AlmaConfiguration.
#   This needs to move somewhere, not sure where yet.
    def GetListofActions(self):
    #    GetProvCodes
    #    GetPermanentLocations(library, url, APIKey)
        try:
            self.ActionList = []
            try:
                if AlmaManagerConfiguration.instance.API_LOCATIONS:
                    fetched = alma.GetPermanentLocations(AlmaManagerConfiguration.instance.default_library, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                    for each in fetched:
                        self.ActionList.append(Action(ActionType.PERMANENT_LOCATION, each[0].text, each[1].text))
                if AlmaManagerConfiguration.instance.API_PROVENANCES:
                    fetched = alma.GetProvCodes(AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                    for each in fetched[6]:
                        self.ActionList.append(Action(ActionType.PROVENANCE_CODE, each[0].text, each[1].text))
            except Exception:
                self.ActionsAvail.emit(self.ActionList)
                raise Exception("Unable to Fetch Actions from API")
            self.ActionsAvail.emit(self.ActionList)
        except Exception as e:
            self.ActionsError.emit(str(e))
