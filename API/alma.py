from urllib.request import Request, urlopen
from urllib.parse import urlencode, quote_plus
from Types.Item import Item
import xml.etree.ElementTree as ET
import sys
import os


class alma:
#   This function gets item label printing data using a barcode.
    def GetItemDataByBarcode(barcode, url, APIKey):
        fetchURL = (url
                    + 'items?item_barcode='
                    + barcode
                    + '&apikey='
                    + APIKey)
        request = Request(fetchURL)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
#        print(response_body)
        retVal = ET.fromstring(response_body)
        return retVal

#   This function uploads an Item to the currently defined alma object
    def PutData(_item, url, APIKey):
        if not Item.IsItem(_item):
            raise TypeError("_item must be an Item Object")
        fetchURL = (url
                    + 'bibs/'
                    + _item.mms_id
                    + '/holdings/'
                    + _item.holding_id
                    + '/items/'
                    + _item.item_pid)
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey})
        values = _item.tostring()
        headers = {'Content-Type': 'application/xml'}
        request = Request(fetchURL + queryParams, data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response_body = urlopen(request).read()
        return response_body

    #   This function gets bib data using mms_id, holding_id, and item_pid
    def GetItemDataByMMSandHolding(mms_id, holding_id, item_pid, url, APIKey):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    + '/holdings/'
                    + holding_id
                    + '/items/'
                    + item_pid
                    )
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey  })
        request = Request(fetchURL + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal

    def GetItemRepresentations(mms_id, url, APIKey):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    + '/representations'
                    )
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey  })
        request = Request(fetchURL + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = response_body
        return retVal

    def PutHolding(_item, url, APIKey):
        if not Item.IsItem(_item):
            raise TypeError("_item must be an Item Object")
        fetchURL = (url
                    + 'bibs/'
                    + _item.mms_id
                    + '/holdings/'
                    + _item.holding_id
                    + '/items/'
                    + _item.item_pid)
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey})
        values = _item.tostring()
        headers = {'Content-Type': 'application/xml'}
        request = Request(fetchURL + queryParams, data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response_body = urlopen(request).read()
        return response_body

    def GetHolding(mms_id, holding_id, url, APIKey):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    + '/holdings/'
                    + holding_id
                    )
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey  })
        request = Request(fetchURL + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal

    def GetBib(mms_id, url, APIKey):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    )
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey})
        request = Request(fetchURL + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal
#   This program updatesa Bib using the Item object passed
    def DisconnectFromNZ(mms_id, url, APIKey, Bib):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    )
        queryParams = '?' + urlencode({quote_plus('op'): 'unlink_from_nz', quote_plus('apikey'): APIKey})
        headers = {'Content-Type': 'application/xml'}
        values = ET.tostring(Bib)
        request = Request(fetchURL + queryParams, data=values, headers=headers)
        request.get_method = lambda: 'POST'
        response_body = urlopen(request).read()
        return response_body

    def PutBib(mms_id, url, APIKey, Bib):
        fetchURL = (url
                    + 'bibs/'
                    + mms_id
                    )
        queryParams = '?' + urlencode({quote_plus('apikey'): APIKey})
        headers = {'Content-Type': 'application/xml'}
        values = ET.tostring(Bib)
        request = Request(fetchURL + queryParams, data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response_body = urlopen(request).read()
        return response_body

#   This function gets the List of Libraries from the API
    def GetLibraries(url, APIKey):
        url = url + 'conf/libraries'
        queryParams = ('?'
                       + urlencode({quote_plus('apikey'):
                                    APIKey}))
        request = Request(url + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal

#   This function gets the currently available Permanent Locations
#   from the currently defined Library, or takes a library as an argument
    def GetPermanentLocations(library, url, APIKey):
        url = (url + 'conf/libraries/'
               + library + '/locations')
        queryParams = ('?' + urlencode({quote_plus('apikey'):
                                        APIKey}))
        request = Request(url + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal

#   This function fetches code-tables/provenanceCodes, Which contains
#   all posible Provenance Codes
    def GetProvCodes(url, APIKey):
        url = url + 'conf/code-tables/provenanceCodes'
        queryParams = ('?' + urlencode({quote_plus('apikey'):
                                        APIKey}))
        request = Request(url + queryParams)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        retVal = ET.fromstring(response_body)
        return retVal

