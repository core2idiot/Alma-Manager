from API.alma import alma
from PyQt5.QtCore import QThread, pyqtSignal
import time
from Administration.AlmaManagerConfiguration import AlmaManagerConfiguration
from Types.ActionType import ActionType
class ActionHandler(QThread):
    ActionStarted = pyqtSignal()
    ActionDone = pyqtSignal()
    ActionError = pyqtSignal(str)
    def __init__(self):
        super(ActionHandler, self).__init__()
        self.ActionToDo = None
        self.Item = None
    def SetupAction(self, ActionToDo, Item):
        self.ActionToDo = ActionToDo
        self.Item = Item
    def run(self):
#       Emit a Signal indicating the Action has started
#       Find type of action
#       Using Settings from AlmaManagerConfiguration Perform Action
#       Emit a Signal indicating the Action has finished successfully or not
        try:
            if AlmaManagerConfiguration.instance.DebugMode:
                time.sleep(1)
                self.ActionToDo = None
                self.Item = None
                self.ActionDone.emit()
            else:
                if self.ActionToDo.Type == ActionType.PERMANENT_LOCATION:
                    self.Item.AllData[2].find('location').text = self.ActionToDo.MachineReadable
                    alma.PutData(self.Item, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)

                elif self.ActionToDo.Type == ActionType.PROVENANCE_CODE:
                    self.Item.AllData[2].find('provenance').text = self.ActionToDo.MachineReadable
                    alma.PutData(self.Item, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                elif self.ActionToDo.Type == ActionType.SUPPRESS:
                    Bib = alma.GetBib(self.Item.mms_id, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                    Bib = alma.DisconnectFromNZ(self.Item.mms_id, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey, Bib)
                    Bib = alma.GetBib(self.Item.mms_id, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey)
                    Bib.find('suppress_from_publishing').text = 'true'
                    Bib = alma.PutBib(self.Item.mms_id, AlmaManagerConfiguration.instance.url, AlmaManagerConfiguration.instance.ApiKey, Bib)
                else:
                    raise Exception("Invalid Option")
                self.ActionToDo = None
                self.Item = None
                self.ActionDone.emit()

        except Exception as e:
            self.ActionError.emit(str(e))

