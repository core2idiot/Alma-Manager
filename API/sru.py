from urllib.request import Request, urlopen
from lxml import etree
class sru:
    def GetSRUsearchRetrieveByMMSID(mms_id, Zone, SRU_URL):
        fetchURL = (SRU_URL
                    + "/view/sru/"
                    + Zone #IZ or NZ
                    + "?version=1.2&operation=searchRetrieve&query=alma.mms_id="
                    + mms_id)
        request = Request(fetchURL)
        request.get_method = lambda: 'GET'
        response_body = urlopen(request).read()
        #print(response_body)
        retVal = etree.fromstring(response_body)
        return retVal

    def GetSRUAttribute(SRUData, Findtag, Findcode):
        retVal = []
        for element in SRUData.iter('*'):
            tag = element.get('tag')
            if tag == Findtag:
                for ele in element.iter('*'):
                    code = ele.get('code')
                    if code == Findcode:
                        for e in ele.iter('*'):
                            retVal.append(e)
                        return retVal
        raise Exception("Unable to find Data")

    def findNZ_MMSIDByIZ_MMSID(IZ_mms_id, IZ_Name, NZ_Name, SRU_URL):
        iz_Response = sru.GetSRUsearchRetrieveByMMSID(IZ_mms_id, IZ_Name, SRU_URL)
        for element in iz_Response.iter('*'):
            tag = element.get('tag')
            if tag == '035':
                for ele in element.iter('*'):
                    code = ele.get('code')
                    if code == 'a':
                        for e in ele.iter('*'):
                            if NZ_Name in e.text:
                                retVal = e.text
                                retVal = retVal.replace(NZ_Name, '')
                                for char in retVal:
                                    if char == ')':
                                        retVal = retVal.replace(char, '')
                                        break
                                    else:
                                        retVal = retVal.replace(char, '')
                                return retVal
        raise Exception("Unable to find NZ MMS ID")

    def FindCurrentAllianceHoldingsBYNZ_MMSID(NZ_MMSID, NZ_Name, SRU_URL):
        retVal = []
        holdings = sru.GetSRUsearchRetrieveByMMSID(NZ_MMSID, NZ_Name, SRU_URL)
        for elements in holdings.iter('*'):
            if(elements.get('tag') == '852'):
                for ele in elements.iter('*'):
                    if ele.get('code') == 'a':
                        retVal.append(ele.text)
        return retVal

    def FindCurrentAllianceHoldingsBYIZ_MMSID(IZ_MMSID, IZ_Name, NZ_Name, SRU_URL):
        NZ_MMSID = sru.findNZ_MMSIDByIZ_MMSID(IZ_MMSID, IZ_Name, NZ_Name, SRU_URL)
        return sru.FindCurrentAllianceHoldingsBYNZ_MMSID(NZ_MMSID, NZ_Name, SRU_URL)
