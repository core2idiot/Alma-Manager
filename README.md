# Alma Manager
## Project to Interface with Alma in a more efficent less laborious way
In order to use Alma Manager requires an Alma API key that has access to bibs, items, and conf.

Alma Manager will upon launch attempt to parse a file named AlmaManager.conf
    This file can be located in ~/AlmaManager/AlmaManager.conf or ./AlmaManager.conf.  ~/AlmaManager/AlmaManager.conf will take priority.
### Dependancies
```
/*May be incomplete*/
Python 3.6
PyQt5
lxml
```
### Current Feature List
```
Retrieve and Display information on Item including other libraries that hold the scanned item
Change Permanent Location on Item
Change Provenance Code on Item
```
### Sample AlmaManager.conf
```
#I'm a comment
#Required for base functionality
ALMA_URL = https://api-na.hosted.exlibrisgroup.com/almaws/v1/
API_KEY = 0123456789012346678
DEFAULT_LIBRARY = EXAMPLE_LIBRARY
#Alma Manager Features
LOG_FILE = alma.log
LOG_LEVEL = 10
PRINT_LOG_MSG = yes
#SRU - Network Zone information Enablement
#Warning: This does noticably slow down AlmaManager since it has to a three more API Calls every time it loads an item.
ENABLE_SRU = yes
SRU_INSTITUTION = 01ALLIANCE_KSU
SRU_ALLIANCE = 01ALLIANCE_NETWORK
SRU_URL = https://na01.alma.exlibrisgroup.com
#These automatically populate the ActionPanel with the possible actions for your library
USE_API_FOR_PROVENANCE_CODES = no
USE_API_FOR_PERMANENT_LOCATIONS = no
#Restricting the ActionPanel
#These Options require the machine readable name
#Hide takes precedence over known.
KNOWN_LOCATION = BkStorage
KNOWN_PROVENANCE_CODE = LIS
HIDE_PROVENANCE_CODE = LIS
HIDE_LOCATION = BkStorage
```
